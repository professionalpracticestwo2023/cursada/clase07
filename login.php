<?php 
session_start();
$usuarios="usuarios.dat";
$login = htmlspecialchars(trim($_POST['login']));
$pass = sha1(trim($_POST['pass'])); 
if (empty($login) or empty($pass))
{
    header("Location:index.php");
}
else
{
    if (file_exists($usuarios)) 
    {
        $verifica=0;
        $archivo=fopen($usuarios,"r") or die ("Error");
        while (!feof($archivo) and $verifica==0){
            $linea=fgets($archivo);
            $user=explode("|",$linea);
            
            if (strcmp($user[0],$login)== 0 and strcmp($user[1],$pass)==0)
            {
                $_SESSION["login"]=$user[0];
                $_SESSION["nombre"]=$user[2];
                $_SESSION["apellido"]=$user[3];
                $_SESSION["rol"]=$user[4];
		        $_SESSION["estado"]=$user[5];
                $verifica=1;
                break;
            }
        }
        if ($verifica==1){
            header("Location:dashboard.php");
        } 
        else {
		        echo "Login o Password Incorrectos";  
        }
    } 
    else {
        header("Location:index.php");
    }

}
       
?>